import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const LoginForm = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post('/login', formData, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      console.log(res.data);
    } catch (err) {
      console.error(err);
    }
  };

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <form className="LoginForm" method="POST" encType="application/x-www-form-urlencoded" onSubmit={handleSubmit}>
      <label>Email:</label>
      <input type="email" name="email" onChange={handleInputChange} />

      <label>Password:</label>
      <input type="password" name="password" onChange={handleInputChange} />

      <button type="submit">Connexion</button>
      <Link to="/inscription">Vous n'avez pas de compte ? Inscrivez-vous</Link>
    </form>
  );
};

export default LoginForm;
