import { Link } from "react-router-dom";
import L, { popup } from 'leaflet';
import React, { useEffect, useState } from 'react';
import "leaflet/dist/leaflet.css";

function Home() {
  const [hoverData, setHoverData] = useState(null);
  const [result, setResult] = React.useState([]);

  const search = (event) => {
    event.preventDefault();
    const recherche = document.getElementById("insee");
    const resultats = recherche.value;
    console.log("Recherche effectuée avec le code postal : ", resultats);

    fetch(`http://127.0.0.1:8001/loyer/${resultats}`)
      .then((response) => {
        console.log("Réponse du serveur reçue : ", response);
        return response.json();
      })
      .then((data) => {
        console.log("Données obtenues : ", data);
        setResult(data);
      })
      .catch((error) => {
        console.error("Erreur lors de la requête : ", error);
        alert("Erreur : " + error);
      });
  };

  useEffect(() => {
    const map = L.map("map").setView([47, 2], 5);

    fetch("https://france-geojson.gregoiredavid.fr/repo/departements.geojson")
      .then((response) => response.json())
      .then((data) => {
        L.geoJSON(data, {
          style: { color: "#333", weight: 1 },
          onEachFeature: function (feature, layer) {
            layer.on({
              mouseover: function (e) {
                var popup = L.popup();
                layer.setStyle({ color: "red" });
                fetch(`http://127.0.0.1:8001/loyerdepartement/` + feature.properties.code)
                  .then((response) => response.json())
                  .then((data) => {
                    popup.setContent('<h3>' + feature.properties.nom + '-' + feature.properties.code + '</h3>'
                      + '<h3>Prix moyen au mètre carré :</h3><p>' + data.moyennett + ' €</p>' + '<h3>Note d\'habitabilité :</h3><p> ' + data.note + '/5</p>');
                    console.log(data); // Vérifiez si les données sont récupérées
                    setHoverData(data);
                    popup.setLatLng(e.latlng);
                    popup.openOn(map);
                  });

              },

              mouseout: function (e) {
                setHoverData(null);
                layer.setStyle({ color: "#333" });
              },
            });
          },
        }).addTo(map);
      });

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: "&copy; OpenStreetMap contributors",
    }).addTo(map);
    

    return () => {
      map.remove(); // Supprimez la carte Leaflet lorsque vous quittez la page
    };
  }, []);
  
    return (
        <div>
          <div class="titre1">
            <h2>Faites votre recherche ici</h2>
          </div>
          <div class="carte">
            <div>
                <div id="map"></div>
            </div>
            <div className="container">
              <form>
                <label for="insee">Entrez le code insee de la commune :</label><br/>
                <input type="text" id="insee" placeholder="Code insee"></input>
                <button onClick={search}>Recherchez</button>
              </form>
              <h2>Infos sur la commune</h2>
              <div>
                <div>
                  <ul>
                  {result.map((item) => (
                    <div key={item.insee}>
                      <h3>Nom de la commune :</h3>
                      <li>{item.nomcommune}</li>
                      <h4>Prix moyen au mètre carré :</h4>
                      <li>{item.average} €</li>
                      <h4>Taux de délits :</h4>
                      <li>{item.tauxDelits}</li>
                      <h4>Note d'habitabilité :</h4>
                      <li>{item.note}/5</li>
                    </div>
                  ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="titre">
            <h4>Les données expliquées</h4>
          </div>
          <div class="text">
            <p>
            Notre application web a pour but d’aider les gens à trouver le lieu idéal pour habiter en France en se basant sur deux critères essentiels : la sécurité et le coût de la vie.

La première fonctionnalité de l’application est la recherche par code insee des communes. Les utilisateurs peuvent saisir le nom de la commune qu’ils souhaitent explorer dans la barre de recherche, ce qui leur donnera accès à une fiche détaillée de la commune. Cette fiche comprendra une note d’habitabilité, qui prend en compte le nombre de crimes rapportés au mètre carré pour cette commune. Plus la note est élevée, plus la commune est considérée comme sûre.

La seconde fonctionnalité de l’application est la carte interactive par département. Les utilisateurs peuvent explorer les différents départements de France sur la carte et cliquer sur chaque commune pour accéder à sa fiche détaillée. Cette carte donne également une indication du niveau de sécurité de chaque commune en France, ainsi que le prix moyen du loyer au mètre carré dans cette commune. Ces informations sont mises à jour régulièrement pour garantir leur précision.

En résumé, notre application web permet aux utilisateurs de comparer différentes communes en France en termes de sécurité et de coût de la vie. Avec notre note d’habitabilité et nos informations sur les prix des loyers, les utilisateurs peuvent prendre une décision éclairée quant à l’endroit où ils souhaitent habiter en France.


            </p>
          </div>
        </div>
    );
  }
  
  export default Home;