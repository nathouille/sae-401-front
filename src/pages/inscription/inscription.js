import React, { useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';

const RegisterForm = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const formData = new FormData();
      formData.append('email', formData.email);
      formData.append('password', formData.password);
  
      const res = await axios.post('http://localhost:8000/register', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
  
      console.log(res.data);
  
      // Si l'utilisateur a été enregistré avec succès, rediriger vers la page d'accueil
      navigate('/accueil');
    } catch (err) {
      console.error(err);
    }
  };

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <form className="RegisterForm" method="POST" encType="multipart/form-data" onSubmit={handleSubmit}>
      <label>Email:</label>
      <input type="email" name="email" onChange={handleInputChange} />

      <label>Password:</label>
      <input type="password" name="password" onChange={handleInputChange} />

      <button type="submit">Inscription</button>
      <Link to="/connexion">Avez-vous déjà un compte ? Connectez-vous</Link>
    </form>
  );
};

export default RegisterForm;
