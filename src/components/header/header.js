import { Link } from "react-router-dom";
import React from 'react';
import style from './header.module.scss';

function Header({ onDarkModeToggle, isDarkMode }) {

    const headerClasses = `${style.header} ${isDarkMode ? style.darkMode : ''}`;

    return (
        <>  
            <header className={headerClasses}>
                <div>         
                    <Link to="../inscription" relative="path">
                    <img className={style.user} src='../../img/utilisateur.png' alt="menu"></img>
                    </Link>
                </div>
                <div>
                    <Link to="../" relative="path">
                    <h1 className={`${style.couleur} ${style.crimes}`}>Crimes²</h1>
                    </Link>   
                </div>
                <div>
                    <button className={style.button} onClick={onDarkModeToggle}>
                        {isDarkMode ? 'Light Mode' : 'Dark Mode'}
                    </button>
                </div>
            </header>
        </>
    );
}

export default Header;
