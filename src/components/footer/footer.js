import { Link, Outlet } from "react-router-dom";
import React from 'react';
import style from './footer.module.scss';

function Footer() {
    return (
        <>
            <footer className={style.footer}>
                <div>
                    <Link to="../" relative="path">
                        <h4 className={style.couleur}>Accueil</h4>
                    </Link>
                </div>
                <div className={style.tel}>
                    <p>0404040404</p>
                    <p>crimecarre@france.fr</p>
                </div>
                <div>
                <Link to="https://www.instagram.com/nathan_masson_/?hl=fr" target="_blank" rel="noopener noreferrer">
                    <img className={style.image} src='../../img/instagram.png' alt="instagram" />
                </Link>
                <Link to="https://www.facebook.com/nathan.masson.967/" target="_blank" rel="noopener noreferrer">
                    <img className={style.image} src='../../img/facebook.png' alt="facebook"></img>
                </Link>
                <Link to="https://www.tiktok.com/@nathan_masson?lang=fr" target="_blank" rel="noopener noreferrer">
                    <img className={style.image} src='../../img/tik-tok.png' alt="tiktok"></img>
                </Link>
                </div>
            </footer>
        </>
    );
}

export default Footer;