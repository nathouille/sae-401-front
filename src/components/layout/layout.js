import React, { useState } from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';
import { Link, Outlet } from "react-router-dom";
import style from './layout.module.scss';

function Layout({ children }) {
    const [isDarkMode, setIsDarkMode] = useState(false);
    const handleDarkModeToggle = () => {
        setIsDarkMode(!isDarkMode);
    }

    const layoutClasses = `${style.layout} ${isDarkMode ? style.darkMode : ''}`;

    return (
        <>
            <div className={layoutClasses}>
                <Header onDarkModeToggle={handleDarkModeToggle} isDarkMode={isDarkMode} />
                <main>
                    {children}
                </main>
                <Footer />
            </div>
        </>
    );
}

export default Layout;
